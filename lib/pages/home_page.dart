import 'package:basic_components/pages/chat_page.dart';
import 'package:basic_components/pages/images_page.dart';
import 'package:basic_components/widgets/avatar.dart';
import 'package:basic_components/widgets/bottom_menu.dart';
import 'package:basic_components/widgets/cronometer.dart';
import 'package:basic_components/widgets/my_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  static final routeName = 'home';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _isEnabled = false;
  double _fontSize = 20.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: SafeArea(
        child: BottomMenu(
          items: [
            BottomMenuItem(
                iconPath: 'assets/icons/inicio.svg', label: 'Inicio'),
            BottomMenuItem(
                iconPath: 'assets/icons/historial.svg', label: 'Historial'),
            BottomMenuItem(
                iconPath: 'assets/icons/ofertas.svg', label: 'Ofertas'),
            BottomMenuItem(
                iconPath: 'assets/icons/perfil.svg', label: 'Mi Perfil')
          ],
        ),
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Color(0xffffffff),
          child: Column(
            children: [
              MyAppbar(
                leftIcon: 'assets/icons/camera.svg',
                rightIcon: 'assets/icons/messenger.svg',
                onRightClick: () {
                  final route = MaterialPageRoute(
                      builder: (BuildContext _) => ChatPage());
                  Navigator.push(context, route);
                },
                onLeftClick: () {
                  Navigator.pushNamed(context, 'images', arguments: ImagesPageArgs(username: 'Jhonatan Tupayachi', isActive: true));
                },
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Avatar(),
                    SizedBox(
                      height: 20,
                    ),
                    Text('Bienvenido'),
                    Text(
                      'Jhonatan Tupayachi',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Container(
                      width: 100,
                      height: 1,
                      margin: EdgeInsets.symmetric(vertical: 20),
                      color: Color(0xffaaaaaa),
                    ),
                    _isEnabled == true
                        ? Cronometer(
                            initTime: 90.0,
                            fontSize: _fontSize,
                          )
                        : Container(),
                    CupertinoButton(
                      color: Colors.blue,
                      child: Text('isEnabled: $_isEnabled'),
                      onPressed: () {
                        setState(() {
                          _isEnabled = !_isEnabled;
                        });
                      },
                    ),
                    SizedBox(height: 20),
                    CupertinoButton(
                      color: Colors.green,
                      child: Text('Navigate to chat'),
                      onPressed: () {},
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
