import 'package:basic_components/widgets/my_appbar.dart';
import 'package:flutter/material.dart';

class ImagesPageArgs {
  final String username;
  final bool isActive;

  ImagesPageArgs({@required this.username, @required this.isActive});
}

class ImagesPage extends StatefulWidget {
  static final routeName = 'images';

  @override
  _ImagesPageState createState() => _ImagesPageState();
}

class _ImagesPageState extends State<ImagesPage> {
  @override
  Widget build(BuildContext context) {
    final ImagesPageArgs args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        body: SafeArea(
            child: Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(children: [
        MyAppbar(
          leftIcon: 'assets/icons/back.svg',
          rightIcon: 'assets/icons/messenger.svg',
          onLeftClick: () => Navigator.pop(context),
        ),
        Expanded(
          child: Text(args.username),
        )
      ]),
    )));
  }
}
