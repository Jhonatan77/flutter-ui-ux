import 'package:basic_components/widgets/my_appbar.dart';
import 'package:flutter/material.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(children: [
        MyAppbar(
          leftIcon: 'assets/icons/back.svg',
          rightIcon: 'assets/icons/messenger.svg',
          onLeftClick: () => Navigator.pop(context),
        ),
        Expanded(
          child: Text('Chat Page'),
        )
      ]),
    )));
  }
}
