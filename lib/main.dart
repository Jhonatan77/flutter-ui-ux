import 'package:basic_components/pages/home_page.dart';
import 'package:basic_components/pages/images_page.dart';
import 'package:basic_components/pages/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      routes: {
        HomePage.routeName: (BuildContext context)=>HomePage(),
        ImagesPage.routeName: (BuildContext context)=>ImagesPage() 
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashPage(),
    );
  }
}
