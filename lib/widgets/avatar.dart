import 'package:basic_components/widgets/circle_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Avatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      child: Stack(
        children: [
          SvgPicture.asset(
              'assets/icons/user.svg', width: 200,),
          Positioned(
            child: CircleContainer(
              size: 60,
              child: Icon(Icons.edit),
            ),
            right: 0,
            top: 0,
          ),
        ],
      ),
    );
  }
}
