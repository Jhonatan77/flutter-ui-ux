import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:basic_components/widgets/circle_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Cronometer extends StatefulWidget {

  final initTime, fontSize;

  const Cronometer({Key key, this.initTime = 0.0, this.fontSize}) : super(key: key);

  @override
  _CronometerState createState() => _CronometerState();
}

class _CronometerState extends State<Cronometer> with AfterLayoutMixin {

  double _time;

  Timer _timer;

  @override
  void initState() {
    super.initState();
    this._time = widget.initTime;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override void didUpdateWidget(covariant Cronometer oldWidget) {
    super.didUpdateWidget(oldWidget);
    print('Anterior: ${oldWidget.fontSize}');
    print('Nuevo: ${widget.fontSize}');
  }

  @override
  void dispose() {
    super.dispose();
    _stop();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    showDialog(context: context, builder: (BuildContext context) {
      return AlertDialog(title: Text('Hola Mundo'),);
    });
  }

  _start() {
    _timer = Timer.periodic(Duration(seconds: 1), (Timer _) {
      setState(() {
        _time ++;
      });
    });
  }

  _stop() {
    _timer?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        child: Column(children: <Widget>[
          Text(
            _time.toString(),
            style: TextStyle(fontSize: widget.fontSize ?? 30),
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CupertinoButton(
                child: CircleContainer(
                  size: 55,
                  child: Icon(Icons.play_arrow),
                ),
                onPressed: _start,
              ),
              CupertinoButton(
                child: CircleContainer(
                  size: 55,
                  child: Icon(Icons.stop),
                ),
                onPressed: _stop,
              )
            ],
          )
        ]));
  }
}
