import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyAppbar extends StatelessWidget {

  final String rightIcon, leftIcon;
  final VoidCallback onLeftClick, onRightClick;

  MyAppbar({Key key, @required this.rightIcon, @required this.leftIcon, this.onLeftClick, this.onRightClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffffffff),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        CupertinoButton(
          padding: EdgeInsets.all(15),
          child: SvgPicture.asset(
            leftIcon,
            width: 50,
          ),
          onPressed: onLeftClick,
        ),
        Text(
          'LOGO',
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
        Stack(
          children: [
            CupertinoButton(
              padding: EdgeInsets.all(15),
              child: SvgPicture.asset(
                rightIcon,
                width: 50,
              ),
              onPressed: onRightClick,
            ),
            Positioned(
              top: 12,
              right: 12,
              child: Container(
                  width: 20,
                  height: 20,
                  decoration:
                      BoxDecoration(color: Colors.red, shape: BoxShape.circle)),
            )
          ],
        ),
      ]),
    );
  }
}
